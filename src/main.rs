extern crate rand;
extern crate structopt;
extern crate flexi_logger;
#[macro_use]
extern crate log;

use std::fs::File;
use std::io::prelude::*;
use structopt::StructOpt;
use std::collections::HashMap;
use rand::thread_rng;
use rand::Rng;
use std::path::Path;
use rand::rngs::ThreadRng;
#[derive(StructOpt)]
struct Cli {
    #[structopt(parse(from_os_str))]
    path: std::path::PathBuf,
}

type ChainWord = HashMap<String, Vec<String>>;
type Word = String;

fn main() {
    //  Init logger and set vars
    flexi_logger::Logger::with_env_or_str("info")
        .start()
        .unwrap();
    let args: Cli = Cli::from_args();

    // Parse source text to a chain of words
    if let Some(source_text) = get_source(&args.path) {

        // Build the chain of words
        let chain = build_chain(source_text);
        debug!("Chain: {:#?}", &chain);
        // Create a sentence
        let sentence = build_sentence(&chain);

        println!("The Final Sentence: `{}`", sentence);
    }
}

fn get_source(path: &Path) -> Option<String> {
    // Open the file
    let source_text = File::open(path).map_err(|e| {
        eprintln!("Can't open `{}`: {}", path.display(), e)
    });
    if source_text.is_err() { return None; }

    // Read the file
    let mut text = String::new();
    let _ = source_text.unwrap().read_to_string(&mut text).map_err(|e| {
        eprintln!("Can't read source (is it in UTF8?): {}", e);
    });

    // Uniformization
    text = text.replace("'","' ");
    text = text.replace("\n", " "); // Ignore \n
    Some(text)
}

fn build_chain(text: String) -> ChainWord {
    let mut chain: ChainWord = ChainWord::new();

    // Split every word to create the chain
    let mut last_word = Word::new();
    let lines: Vec<&str> = text.split("\n").collect();

    for line in lines {
        last_word = Word::new();

        for word in line.split_whitespace() {
            if last_word != "" || !last_word.contains(".") {
                // Add a new choice for a word in the chain
                let mut possibilities: Vec<Word> = chain.get(&last_word).unwrap_or(&vec!()).clone();
                possibilities.push(word.into());
                chain.insert(last_word, possibilities);
            }
            last_word = word.into();
        }
    }
    chain
}

fn next_word(rng: &mut ThreadRng, chain: &ChainWord, lastword: &Word) -> Word {
    let items = chain.get(lastword)
        .expect("WTF Exception =/")
        .clone();
    items.get(rng.gen_range(0, items.len()))
        .expect("WTF Exception =/")
        .to_string()
}

fn build_sentence(chain: &ChainWord) -> String {
    let mut sentence = String::new();
    let mut rng = thread_rng();
    let mut last_word = Word::new();

    // Append to the final sentence each word
    while !sentence.contains(".") {

        // set the new word
        last_word = next_word(&mut rng, chain, &last_word);
        // add the word to the sentence
        sentence += &(last_word.as_str().to_owned() + " ");
    }
    sentence
}